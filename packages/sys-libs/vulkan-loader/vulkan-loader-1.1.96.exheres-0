# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-Loader tag=v${PV} ] \
    cmake [ api=2 ] \
    python [ blacklist=2 multibuild=false has_bin=false has_lib=false ]

SUMMARY="Vulkan Installable Client Driver (ICD) desktop loader"
DESCRIPTION="
Vulkan is an explicit API, enabling direct control over how GPUs actually work. As such, Vulkan
supports systems that have multiple GPUs, each running with a different driver, or ICD (Installable
Client Driver). Vulkan also supports multiple global contexts (instances, in Vulkan terminology).
The ICD loader is a library that is placed between a Vulkan application and any number of Vulkan
drivers, in order to support multiple drivers and the instance-level functionality that works
across these drivers. Additionally, the loader manages inserting Vulkan layer libraries, such as
validation layers, between an application and the drivers.
"
HOMEPAGE+=" https://www.khronos.org/vulkan"

BUGS_TO="keruspe@exherbo.org"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    X
    wayland
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/vulkan-headers[>=$(ever range 1-3)]
        X? (
            x11-libs/libxcb
            x11-libs/libX11
        )
        wayland? ( sys-libs/wayland )
        !sys-libs/vulkan [[
            description = [ The vulkan package has been split into individual packages ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DVulkanRegistry_DIR:PATH=/usr/share/vulkan/registry
    -DBUILD_LOADER:BOOL=TRUE
    -DBUILD_TESTS:BOOL=FALSE
    -DUSE_CCACHE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    "X WSI_XCB_SUPPORT"
    "X WSI_XLIB_SUPPORT"
    "wayland WSI_WAYLAND_SUPPORT"
)

src_install() {
    cmake_src_install

    keepdir /etc/vulkan/explicit_layer.d
    keepdir /etc/vulkan/icd.d
    keepdir /etc/vulkan/implicit_layer.d
}

