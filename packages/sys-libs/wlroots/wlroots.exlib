# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='swaywm' ] meson

SUMMARY="A modular Wayland compositor library"
DESCRIPTION="Pluggable, composable modules for building a Wayland compositor"

LICENCES="MIT"
SLOT="0"

MYOPTIONS="
    caps [[ description = [ for capability support ] ]]
    X [[ description = [ Enable X11 backend and XWayland ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: elogind systemd-logind ) [[
        *description = [ Logind support ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/libinput[>=1.7.0]
        sys-libs/wayland[>=1.15]
        sys-libs/wayland-protocols[>=1.15]
        x11-dri/mesa[wayland][X?]
        x11-dri/libdrm
        x11-libs/libxkbcommon[wayland][X?]
        x11-libs/pixman
        caps? ( sys-libs/libcap )
        X? (
            x11-libs/libxcb
            x11-proto/xcb-proto
            x11-utils/xcb-util-image
            x11-utils/xcb-util-renderutil
        )
        providers:elogind? ( sys-auth/elogind )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        providers:systemd-logind? ( sys-apps/systemd )
    run:
        X? (
            x11-server/xorg-server[xwayland]
        )
    suggestion:
        X? (
            x11-utils/xcb-util-wm
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Drootston=false
    -Dexamples=false
    -Dlogind=disabled
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'caps libcap enabled disabled'
    'X x11-backend enabled disabled'
    'X xwayland enabled disabled'
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dlogind=enabled'
    'providers:elogind -Dlogind-provider=elogind'
    'providers:systemd-logind -Dlogind=enabled'
    'providers:systemd-logind -Dlogind-provider=systemd'
)

