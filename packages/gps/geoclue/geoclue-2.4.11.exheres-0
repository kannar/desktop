# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service vala [ with_opt=true vala_dep=true ]

SUMMARY="GeoClue location framework"
DESCRIPTION="
A software framework which can be used to enable geospatial awareness in applications.  GeoClue uses
DBus to provide location information.
"
HOMEPAGE="https://geoclue.freedesktop.org/"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/releases/${PV:0:3}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    avahi [[ description = [ Enable support for NMEA ] ]]
    gobject-introspection
    gtk-doc
    modem-manager [[ description = [ Build 3G, CDMA and modem GPS backend ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.22]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        core/json-glib[>=0.14]
        dev-libs/glib:2[>=2.44.0]
        gnome-desktop/libsoup:2.4[>=2.42]
        avahi? ( net-dns/avahi[>=0.6.10] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        modem-manager? ( net-wireless/ModemManager[>=1.6] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-demo-agent
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'avahi nmea-source'
    'gobject-introspection introspection'
    gtk-doc
    'modem-manager 3g-source'
    'modem-manager cdma-source'
    'modem-manager modem-gps-source'
    'vapi vala'
)

