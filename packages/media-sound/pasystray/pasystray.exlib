# Copyright 2012-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=christophgysin tag=${PNV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="PulseAudio system tray"
DESCRIPTION="
Pasystray allows setting the default PulseAudio source/sink and moving streams
on the fly between sources/sinks without restarting the client applications.

Features
- switch default sink/source
- move playback/record stream to different sink/source on the fly
- detect pulseaudio instances on the network with avahi
- set X property PULSE_SERVER (like padevchooser's set default server)
- adjust volume/toggle mute of sinks/sources and playback/record streams
- rename devices
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    appindicator    [[ description = [ Use appindicator instead of a standard tray icon ] ]]
    avahi           [[ description = [ Detect PulseAudio daemons using Avahi ] ]]
    libnotify       [[ description = [ Report volume changes to a notification daemon ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2
        media-sound/pulseaudio[>=1.0][avahi?]
        x11-libs/gtk+:3
        x11-libs/libX11
        appindicator? ( dev-libs/libappindicator:0.1 )
        avahi? ( net-dns/avahi[>=0.6] )
        libnotify? ( x11-libs/libnotify[>=0.7] )
    run:
        gnome-desktop/adwaita-icon-theme
    suggestion:
        media-sound/paman       [[ description = [ Launch PulseAudio manager from tray icon ] ]]
        media-sound/pavucontrol [[ description = [ Provides a simple GTK based volume mixer tool ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-statusicon
    --enable-x11
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    appindicator
    avahi
    'libnotify notify'
)

pasystray_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pasystray_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

