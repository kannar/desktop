# Copyright 2013-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# NOTE: find the revision here
# https://dl.google.com/linux/talkplugin/deb/dists/stable/main/binary-i386/Packages

SUMMARY="Google Hangouts (previously Google Talk) Plugin for voice and video chat"
DESCRIPTION="
The Google Hangouts (previously Google Talk) Plugin is a browser plugin that
enables you to use Google voice and video chat to chat face to face with family
and friends.

This product includes software developed by the OpenSSL Project for use in the
OpenSSL Toolkit (http://www.openssl.org/). This product includes cryptographic
software written by Eric Young (eay@cryptsoft.com).
"
HOMEPAGE="https://www.google.com/tools/dlpage/hangoutplugin"

i686_DEB=${PN}_${PV}-1_i386.deb
x86_64_DEB=${PN}_${PV}-1_amd64.deb
DOWNLOADS="
    listed-only:
        https://dl.google.com/linux/talkplugin/deb/pool/main/g/${PN}/${PN}_${PV}-1_amd64.deb
        https://dl.google.com/linux/talkplugin/deb/pool/main/g/${PN}/${PN}_${PV}-1_i386.deb
"

LICENCES="
    BSD-3 [[ note = [ Cryptographic software written by Eric Young (eay@cryptsoft.com). CELT. Opus. ] ]]
    Google-TOS
    openssl [[ note = [ Software developed by the OpenSSL Project ] ]]
"
SLOT="0"
PLATFORMS="-* ~amd64 ~x86"
MYOPTIONS="
    platform: amd64 x86
"

DEPENDENCIES="
    run:
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/pango
"

RESTRICT="strip"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    local target=$(exhost --target)
    deb=${target/-*/_DEB}
    unpack "${!deb}"
    edo tar xf data.tar.gz ./opt/google/talkplugin
}

src_install() {
    # remove cruft
    edo rm -r opt/google/talkplugin/{attributions.txt,cron}

    # install the rest
    insinto /opt/google
    doins -r opt/google/talkplugin
    edo chmod 0755 "${IMAGE}"/opt/google/talkplugin/GoogleTalkPlugin

    # install symlinks for the browser plugins (Netscape/NPAPI version)
    dodir /opt/netscape/plugins
    # Google Talk Plugin NPAPI
    dosym /opt/google/talkplugin/libnpgoogletalk.so /opt/netscape/plugins/libnpgoogletalk.so
    # Google Talk Plugin Video Renderer NPAPI
    dosym /opt/google/talkplugin/libnpo1d.so /opt/netscape/plugins/libnpo1d.so

    # install symlinks for the browser plugins (Chromium/PPAPI version)
    dodir /opt/chromium/pepper
    # Google Talk Plugin PPAPI
    dosym /opt/google/talkplugin/libppgoogletalk.so /opt/chromium/pepper/libppgoogletalk.so
    # Google Talk Plugin Video Renderer PPAPI
    dosym /opt/google/talkplugin/libppo1d.so /opt/chromium/pepper/libppo1d.so

    hereenvd 60googletalk <<EOF
MOZ_PLUGIN_PATH="/opt/netscape/plugins"
COLON_SEPARATED="MOZ_PLUGIN_PATH"
EOF
}

